﻿# Star Wars Api Demo



[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)


> Star Wars API Demo Task with Vanilla JS

## Table of Contents

- [Install](#install)
- [Develop](#develop)
- [Maintainers](#maintainers)
- [Contributing](#contributing)


## Install

```bash
npm install
```

## Develop

Start the Webpack development server.

```bash
npm start
```


## Maintainers

[Philip Aubert (@pauberexperis)](https://gitlab.com/pauberexperis)


## Contributing

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.
