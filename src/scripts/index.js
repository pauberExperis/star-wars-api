import $ from 'jquery'
import 'bootstrap/dist/css/bootstrap.min.css'
import '../styles/index.scss'

/**
 * Global Constants
 */
const planetUrl = "https://swapi.co/api/planets/";


/**
 * Variables
 */
let planets = [];
let pictureNum = 1;
let pictureUrl = "https://starwars-visualguide.com/assets/img/planets/";
let count = 0;

class Planet {
    constructor(id, picture, name, population, climate, terrain, diameter) {
        this.id = id;
        this.picture = picture;
        this.name = name,
        this.population = population;
        this.climate = climate;
        this.terrain = terrain;
        this.diameter = diameter

    }

}

$(document).ready(function () {
    const xhr = $.ajax({
        url: `${planetUrl}`,
        method: 'GET',
        success: function (data) {
            count = data.count;
        }
    }).then(results => postPlanets())
});

function postPlanets() {
    for (let i = 0; i < count + 1; i++) {
        const postPlanets = $.ajax({
            url: `${planetUrl}` + i,
            method: 'GET',
            success: function (data) {

                const $app = $('#app');

                $(data).each((j, planet) => {
                    pictureNum++;
                    console.log(i);
                    //console.log(planet);
                    const newPlanet = new Planet(i, pictureUrl + i  + ".jpg", planet.name, planet.population, planet.climate, planet.terrain, planet.diameter);
                    planets.push(newPlanet);
                    const elPlanet = `
                        <li id = "${newPlanet.id}" class ="list-group-item" margin: 0; padding: 0; width:100px; >
                         <div class="mask flex-center rgba-red-strong">
                         <div class="mainCardImg">
                        <img class ="CardImgMain" src="${newPlanet.picture}" alt="Sorry, Invalid Photo"/>
                         </div>
                        <p>${newPlanet.name}</p>
                         <p>Population: ${newPlanet.population}</p></div>
                        </li>
                        `;
                    $(elPlanet).appendTo('.list-group-flush');
                    $(`#${newPlanet.id}`).click(function (e) {
                        $('.list-group-item').hide();
                        createCard(newPlanet.picture, newPlanet.name, newPlanet.population, newPlanet.climate, newPlanet.terrain, newPlanet.diameter);
                        const button = `<button id="knapp" class="btn btn-info">Go back</button>`;
                        $(button).appendTo('.inner');
                        $(`#knapp`).click(function (e) {
                            $('#knapp').remove();
                            $('.col-4').last().remove();
                            $('.list-group-item').show();
                        });
                    });

                });
            }
        });
    }
}





/**
 * Build a Card template with a picture.
 * @param {string|null} photo
 * @param {string} name
 * @param {string} climate
 * @param {string} population
 * @param {string} diameter
 * @param {string} terrain
 * @returns String
 */
async function createCard(photo, name, population, climate, terrain, diameter) {


    const $app = $('#app');
    $app.append(`
                 <div class="col-4">
                  <div class="card h-100" style="18rem;">
                ${photo
            ? `
                        <div class="card-img">
                            <img class="card-img-top" src="${photo}" alt="Invalid photo"/>
                        </div>
                      ` : ''}
                <div class="card-body">
                    <h5>${name}</h5>
                    <p> population: ${population} </p>  
                    <p> climate: ${climate} </p>
                    <p> terrain: ${terrain} </p>
                    <p> diameter: ${diameter} </p>
                </div>
            </div>
            
        </div>
    `);



};

